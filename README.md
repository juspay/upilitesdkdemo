# Demo Integration for UPI SDK

Use this code base as an example on how to integrate Juspay's UPI SDK with an android app

There are 2 modes this SDK could be used for: Consumer app and Delivery app.

### Consumer
A consumer app would have this SDK in their customer app to allow the customer to make UPI payment using one of 2 methods
1. Send a collect request to the user's UPI ID
2. Send an Android Intent from the merchant app to an installed UPI app to make the payment. (App 2 App switch)

To see sample integration code for this flow, take a look at the classes in the `in.juspay.ec.upi.demo.consumer` package

### Delivery
This mode can be used by delivery executives to collect payments from a customer. This can be done in 2 ways:
1. Display a QR code for the customer to scan and pay with their phone
2. Send a collect request to the user's UPI ID

To see sample integration code for this flow, take a look at the classes in the `in.juspay.ec.upi.demo.delivery` package


## How to follow the code
To start, build the app and run it on an android device

The first screen you see is the `StartActivity`. This screen is just for choosing to see a consumer flow demo or a delivery flow demo. A summary of the steps in each flow is explained below. Follow the code as you click around the demo app to get a clearer picture of the flow.

Finer integration details can be seen in comments inside the Java files.

If you wish, you can make an actual payment (Re. 1) to see the entire SDK flow in action from start to finish.

#### Consumer Flow
The consumer flow in this demo goes like this. Take a look at the comments inside the java files for clarity:


1. When you click on "Start Consumer Demo", `CheckoutActivity` opens up
1. The app will contact our demo merchant server and request for order details (see `onCreate()` of `CheckoutActivity`)
2. Our demo backend creates a simple order with Express Checkout and returns that order and it's details back to this app.
3. The app receives a response from the demo server and will display the demo order summary on a 'checkout page' (see `getOrderDetails()` in `CheckoutActivity`)
4. When the user clicks 'checkout', the app shows a screen (`ChoosePaymentActivity`) which displays 2 methods of payment: Pay with a UPI app (intent) or Send a collect request (in which case the UPI ID needs to be captured before opening the SDK)
5. The method of payment chosen and some details of the Express Checkout order (order ID, merchant ID etc.) are put into a `Bundle` object and passed to the Juspay SDK through `JuspayUPIPayment.start()`.
6. The actual UPI SDK starts here. It opens up and takes control of the screen.
7. After a failed or successful payment, the UPI SDK will return control to the merchant app using the `JuspayUPICallback` object.
8. The demo app gets the callback and immediately sends a request to the demo backend for the payment status
9. The demo backend makes a Server-to-Server call to Express Checkout to confirm the payment status of the order.
10. The final order status is displayed to the user (`PaymentConfirmationActivity`)

#### Delivery Flow
The delivery flow in this demo goes like this. Take a look at the comments inside the java files for clarity:

1. The app will contact our demo merchant server and request for today's deliveries that have to be made (See `onCreate()` inside `DeliveriesActivity`).
2. The demo backend will create 3 simple orders through Express Checkout and return the order details back to the app.
3. The app will display the fetched delivery details as a list on the screen (still inside `DeliveriesActivity`)
4. The user (delivery executive) can tap on any one delivery item when they are ready to collect the amount from the customer.
5. The order details of that payment and put into a `Bundle` object and passed to the Juspay UPI SDK through `JuspayUPIPayment.start()` (see `deliveryItemClick` in `DeliveriesActivity`).
6. The Juspay UPI SDK starts here. It opens up and takes control of the screen and displays a QR code for the user to pay (optionally accepts collect request too).
7. After a failed or successful payment, the UPI SDK will return control to the merchant app using the `JuspayUPICallback` object.
8. The demo app gets the callback and immediately sends a request to the demo backend for the payment status
9. The demo backend makes a Server-to-Server call to Express Checkout to confirm the payment status of the order.
10. The final order status is communicated by the demo merchant app to the delivery executive by displaying a small dialog.
11. In case the Delivery Executive would like to cache the 3 QRs for the 3 deliveries (in case of poor network connectivity at the customer's doorstep), the Executive can tap on the 3 dot menu on the top right corner and click "Cache QRs". This will invoke the UPI SDK with basic order details of the 3 orders and instruct it to Cache the QRs (see `cacheAllQrs()` in `DeliveriesActivity`). Next time an order is tapped, the QR will be loaded from cache rather than being fetched over the network.