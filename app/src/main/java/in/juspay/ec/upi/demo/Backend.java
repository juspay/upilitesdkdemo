package in.juspay.ec.upi.demo;

import android.os.AsyncTask;
import android.util.Log;

import in.juspay.ec.upi.demo.consumer.Order;
import in.juspay.ec.upi.demo.delivery.Delivery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 *  This class makes very simple API calls to our Demo Merchant server. The Demo server makes the
 *  actual Server-to-Server calls to Express Checkout for creating Order IDs with required details
 *  or checking the payment status of any order. API calls included are:
 *
 *      1. getOrderDetails (Consumer app): This API call fetches example checkout details of the
 *          user like order ID, cart items, amount etc.
 *
 *      2. getTodaysDeliveries (Delivery Executive App): This API call gets a list of example
 *          daily deliveries for an executive along with each ones details like order_id,
 *          address etc.
 *
 *      3. getPaymentStatus (Both consumer and delivery app): Sends an order ID to the Demo server
 *          who in turn makes a call to Express Checkout to see the payment status of that order
 */

public class Backend {
    private static final String SERVER_URL = "https://agile-mesa-43027.herokuapp.com";
    private static final String LOG_TAG = "Backend";

    //Possible payment status
    public final static String STATUS_FAIL = "fail";
    public final static String STATUS_CHARGED = "charged";
    public final static String STATUS_PENDING = "pending";

    //Merchant specific values (to be passed to the SDK)
    public static final String MERCHANT_ID = "upi_prod";
    public static final String CLIENT_ID = "default";

    private static PostRequest pendingRequest = null;

    //Listener for getOrderDetails API
    public interface OrderListener {
        void onReceived(Order order);
        void onError(String error);
    }

    //Listener for getPaymentStatus API
    public interface StatusListener {
        void onReceived(String status);
        void onError(String error);
    }

    //Listener for getTodaysDeliveries API
    public interface DeliveriesListener {
        void onReceived(Delivery[] deliveries);
        void onError(String error);
    }

    //Get checkout details of the user (order ID, amount, items)
    public static void getOrderDetails(final OrderListener l) {
        PostRequest request = new PostRequest("/order-details", new PostRequest.ResponseListener() {
            @Override
            public void onResponse(final JSONObject resp) {
                try {
                    final Order o = new Order(resp.getString("order_id"),
                            resp.getString("item_name"),
                            resp.getString("item_id"),
                            resp.getString("item_price"));

                    l.onReceived(o);
                } catch (JSONException e) {
                    Log.e(LOG_TAG, e.toString(), e);
                    l.onError(e.getMessage());
                }
            }

            @Override
            public void onError(final String error) {
                l.onError(error);
            }
        });

        JSONObject requestBody = new JSONObject();
        request.execute(requestBody);
    }

    //Get list of deliveries for the day along with their details (order IDs, address, customer name, item description)
    public static void getTodaysDeliveries(final DeliveriesListener l) {
        PostRequest request = new PostRequest("/todays-deliveries", new PostRequest.ResponseListener() {
            @Override
            public void onResponse(JSONObject resp) {
                try {
                    JSONArray dArray = resp.getJSONArray("deliveries");
                    ArrayList<Delivery> deliveries = new ArrayList<>(dArray.length());
                    for(int i = 0; i < dArray.length(); i++) {
                        JSONObject delivery = dArray.getJSONObject(i);
                        deliveries.add(new Delivery(
                                delivery.getString("order_id"),
                                delivery.getString("customer_name"),
                                delivery.getString("address"),
                                delivery.getString("description"),
                                STATUS_PENDING
                        ));
                    }
                    l.onReceived(deliveries.toArray(new Delivery[deliveries.size()]));
                } catch (Exception e) {
                    l.onError(e.toString());
                    Log.e(LOG_TAG, "Error while extracting deliveries", e);
                }
            }

            @Override
            public void onError(String error) {
                l.onError(error);
            }
        });
        request.execute(new JSONObject());
    }

    //Confirm final payment status after getting 'onComplete' from the SDK
    public static void getPaymentStatus(String orderId, final StatusListener l) {
        PostRequest request = new PostRequest("/order-status", new PostRequest.ResponseListener() {
            @Override
            public void onResponse(JSONObject resp) {
                try {
                    String status = resp.getString("status");
                    if (status.toLowerCase().matches(".*charged.*")) {
                        l.onReceived(STATUS_CHARGED);
                    } else if (status.toLowerCase().matches(".*fail.*")) {
                        l.onReceived(STATUS_FAIL);
                    } else {
                        l.onReceived(STATUS_PENDING);
                    }
                } catch (Exception e) {
                    l.onError(e.toString());
                    Log.e(LOG_TAG, "Failed to parse response", e);
                }
            }

            @Override
            public void onError(String error) {
                l.onError(error);
            }
        });

        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("order_id", orderId);
            request.execute(requestBody);
        } catch (Exception e) {
            l.onError(e.toString());
            Log.e(LOG_TAG, "Error constructing order-status body", e);
        }
    }

    //Makes a post request to the Demo Merchant server
    private static class PostRequest extends AsyncTask<JSONObject, Object, ArrayList<String>> {
        private ResponseListener listener;
        private String error;
        private String host;

        interface ResponseListener {
            void onResponse(JSONObject resp);
            void onError(String error);
        }

        PostRequest(String host, ResponseListener l) {
            this.listener = l;
            this.host = host;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pendingRequest = this;
        }

        @Override
        protected ArrayList<String> doInBackground(JSONObject... jsonObjects) {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(SERVER_URL + this.host).openConnection();
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.connect();

                OutputStream writer = connection.getOutputStream();
                writer.write(jsonObjects[0].toString().getBytes("UTF-8"));
                writer.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                ArrayList<String> response = new ArrayList<>();
                String line;

                while ((line = reader.readLine()) != null) {
                    response.add(line);
                }

                try {
                    writer.close();
                    reader.close();
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Connection close error", e);
                }

                Log.d(LOG_TAG, "PostRequest Returning: " + response.get(0));

                return response;
            } catch (Exception e) {
                Log.e(LOG_TAG, e.toString(), e);
                this.error = e.getMessage();
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> response) {
            pendingRequest = null;
            if(response == null) {
                Log.d(LOG_TAG, "PostRequest onPostExecute got: null");
                listener.onError(this.error);
            } else {
                Log.d(LOG_TAG, "PostRequest onPostExecute got: " + response.toString());
                try {
                    JSONObject o = new JSONObject(response.get(0));
                    if(o.has("error")) {
                        listener.onError("Backend error: " + o.getString("error"));
                    } else {
                        listener.onResponse(o);
                    }
                } catch (JSONException e) {
                    listener.onError(e.getMessage());
                }
            }
        }
    }

    public static void cancel() {
        if(pendingRequest != null) {
            pendingRequest.cancel(true);
            pendingRequest = null;
        }
    }
}
