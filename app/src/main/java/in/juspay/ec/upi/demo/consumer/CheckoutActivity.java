package in.juspay.ec.upi.demo.consumer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import in.juspay.ec.upi.demo.R;
import in.juspay.ec.upi.demo.Backend;

import java.text.DecimalFormat;

/**
 *      This activity fetches the user's cart details and displays an example checkout page with
 *      Items and price.
 */

public class CheckoutActivity extends AppCompatActivity {
    public final double DELIVERY_CHARGE = 35;
    public Order order = null;
    private static final int CHECKOUT_ACTIVITY_CODE = 29;
    private ProgressDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        //Click listener for the "Checkout" button. Starts the ChoosePaymentActivity
        findViewById(R.id.button_checkout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { checkout(); }
        });

        //Get order details from the Demo Merchant Server
        if(order == null) {
            showWaitingDialog();
            getOrderDetailsFromServer();
        }
    }




    //If the user clicks the checkout button, move to ChoosePaymentActivity where they can choose a
    // payment method (intent/collect) and use the SDK.
    private void checkout() {
        Intent intent = new Intent(this, ChoosePaymentActivity.class);
        intent.putExtra("order_id", order.orderId);
        startActivityForResult(intent, CHECKOUT_ACTIVITY_CODE);
    }



    //Gets the order details from the Backend (demo merchant server)
    private void getOrderDetailsFromServer() {
        Backend.getOrderDetails(new Backend.OrderListener() {
            @Override
            public void onReceived(Order order) {
                CheckoutActivity.this.order = order;
                setOrderDetails(order);
                waitingDialog.dismiss();
            }

            @Override
            public void onError(String error) {
                waitingDialog.dismiss();
                showErrorDialog(error);
            }
        });
    }




//---------------------------------------UI HELPERS ------------------------------------------


    //Update the UI with strings of the Order fetched from the server
    @SuppressLint("SetTextI18n")
    private void setOrderDetails(Order o) {
        TextView t = findViewById(R.id.item_name);
        t.setText(o.itemName);

        t = findViewById(R.id.order_id);
        t.setText(o.orderId);

        t = findViewById(R.id.item_id);
        t.setText(o.itemId);

        t = findViewById(R.id.total);
        t.setText(o.price);

        double total = Double.parseDouble(o.price) + DELIVERY_CHARGE;
        double discount = total - 1;

        t = findViewById(R.id.discount);
        t.setText("-" + String.valueOf(discount));

        t = findViewById(R.id.grand_total);
        t.setText(new DecimalFormat("#.00").format(total - discount));

    }



    //Show a waiting dialog when fetching orders from backend
    private void showWaitingDialog() {
        waitingDialog = new ProgressDialog(this);
        waitingDialog.setMessage("Fetching order details...");
        waitingDialog.setCanceledOnTouchOutside(false);
        waitingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Backend.cancel();
                finish();
            }
        });
        waitingDialog.show();
    }



    //Show an error dialog with some message
    private void showErrorDialog(String error) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(CheckoutActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Failed to fetch order");
        dialog.setMessage("Error: " + error);
        dialog.setNeutralButton("BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) { finish(); }
        });
        dialog.show();
    }



    //If the payment was successful (resultCode == RESULT_OK), this activity is closed and we go
    // back to StartActivity. Otherwise, stay here to attempt checkout again.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECKOUT_ACTIVITY_CODE && resultCode == RESULT_OK) {
            finish();
        }
    }
}
