package in.juspay.ec.upi.demo;

import android.app.Application;

import in.juspay.ec.upi.sdk.JuspayUPIPayment;

/**
 * Created by george.varghese on 23/05/18.
 */

/**
 * Calling JuspayUPIPayment.preInit() inside Application.onCreate() ensures that any new config
 * updates or bug fixes for UPI SDK will be downloaded and cached well before the user opens the
 * SDK.
 *
 * Make sure to also inlcude a reference to this Application class inside your AndroidManifest.xml
 * (look for the android:name attribute inside <application> of the AndroidManifest.xml of this
 * demo project)
 */

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        JuspayUPIPayment.preInit(this);
    }
}
