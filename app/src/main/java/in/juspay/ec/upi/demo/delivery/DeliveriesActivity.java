package in.juspay.ec.upi.demo.delivery;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import in.juspay.ec.upi.demo.Backend;
import in.juspay.ec.upi.demo.R;

import org.json.JSONArray;
import org.json.JSONObject;

import in.juspay.ec.upi.sdk.Environment;
import in.juspay.ec.upi.sdk.JuspayConstants;
import in.juspay.ec.upi.sdk.JuspayUPICallback;
import in.juspay.ec.upi.sdk.JuspayUPIPayment;

import static in.juspay.ec.upi.demo.Backend.STATUS_CHARGED;
import static in.juspay.ec.upi.demo.Backend.STATUS_FAIL;

public class DeliveriesActivity extends AppCompatActivity {
    private static final String LOG_TAG = "DeliveriesActivity";

    private ProgressDialog waitingDialog;
    private Delivery[] deliveries = null;
    private int lastOpenedDelivery;
    private DeliveryListAdapter deliveryListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deliveries);


        //Fetch deliveries from demo merchant server
        if(deliveries == null) {
            showWaitingDialog("Fetching today's deliveries...");
            getTodaysDeliveries();
        }
    }



//---------------------------------- DYNAMIC QR PAYMENT --------------------------------------------


    //When a delivery is clicked, take its order ID and start the SDK for a QR payment
    private void deliveryItemClick(Delivery delivery, int i) {

        //Ignore if this order has already been paid for
        if(delivery.paymentStatus.equals(STATUS_CHARGED)) {
            showDialog("Delivery already paid");
            return;
        }

        lastOpenedDelivery = i;

        //Make a new bundle object. We will fill this with required params for the QR payment
        Bundle juspayParams = new Bundle();


        //Required params
        juspayParams.putString(JuspayConstants.MERCHANT_ID, Backend.MERCHANT_ID);   //Your merchant ID with Express Checkout
        juspayParams.putString(JuspayConstants.CLIENT_ID, Backend.CLIENT_ID);       //Your client ID for this platform with Express Checkout
        juspayParams.putString(JuspayConstants.ORDER_ID, delivery.orderId);             //The Express Checkout order ID
        juspayParams.putString(JuspayConstants.ENVIRONMENT, Environment.PRODUCTION);//Set as PROD or Sandbox endpoint for Express Checkout API calls made in the SDK

        //Set the description of the delivery
        juspayParams.putString(JuspayConstants.DESCRIPTION, delivery.description);

        //Tell the SDK to start a QR method of payment
        juspayParams.putString(JuspayConstants.UPI_PAYMENT_METHOD, JuspayConstants.DYNAMICQR.LIB);


        //Dim the screen
        dimScreen();

        //The SDK could take a second to start up. Prevent the user from tapping any buttons before it takes over the screen
        disableTouch();

        //Start the sdk with prepared bundle and callback object (defined below)
        JuspayUPIPayment.start(this, juspayParams, juspayUPICallback);
    }




// ----------------------------------- CALLBACK OBJECT --------------------------------------------



    //Callback object to give SDK. When payment is done/aborted, SDK will call this object
    private JuspayUPICallback juspayUPICallback = new JuspayUPICallback() {
        @Override
        public void onError(@Nullable JSONObject jsonObject) {
            if (jsonObject != null) Log.e(LOG_TAG, "Juspay SDK error: " + jsonObject.toString()); //Check for null to be safe

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {      //Callbacks can happen on another thread. Do your UI work on the UI thread
                @Override
                public void run() {
                    //Remove the screen dimmer and show a waiting dialog while we ask our Demo backend
                    // to confirm the payment status
                    removeScreenDimmer();
                    enableTouch();
                    showWaitingDialog("Confirming payment...");
                }
            });

            //Ask demo merchant server to check payment status with Express Checkout
            Backend.getPaymentStatus(deliveries[lastOpenedDelivery].orderId, statusListener);
        }

        //Called in case the user pressed the back button and wanted to cancel the transaction
        @Override
        public void onBackPressed(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {      //Callbacks can happen on another thread. Do your UI work on the UI thread
                @Override
                public void run() {
                    //Remove the screen dimmer and show a waiting dialog while we ask our Demo backend
                    // to confirm the payment status
                    enableTouch();
                    removeScreenDimmer();
                    showWaitingDialog("Confirming payment...");
                }
            });

            //Ask demo merchant server to check payment status with Express Checkout
            Backend.getPaymentStatus(deliveries[lastOpenedDelivery].orderId, statusListener);
        }

        //Called when the payment is complete (Note: 'complete' here means the payment was either a success or a failure! Confirm with Express Checkout)
        @Override
        public void onCompleted(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {      //Callbacks can happen on another thread. Do your UI work on the UI thread
                @Override
                public void run() {
                    //Remove the screen dimmer and show a waiting dialog while we ask our Demo backend
                    // to confirm the payment status
                    enableTouch();
                    removeScreenDimmer();
                    showWaitingDialog("Confirming payment...");
                }
            });

            //Ask demo merchant server to check payment status with Express Checkout
            Backend.getPaymentStatus(deliveries[lastOpenedDelivery].orderId, statusListener);
        }
    };





//------------------------------- CACHE QRs -------------------------------------------------------



    //Pass the order IDs of all the deliveries to SDK so that the QRs can be cached in case no
    //network is available at time of delivery
    private void cacheAllQrs() {

        //Make a new bundle object. We will fill this with required params for the QR payment
        Bundle juspayParams = new Bundle();

        //Required params
        juspayParams.putString(JuspayConstants.MERCHANT_ID, Backend.MERCHANT_ID);   //Your merchant ID with Express Checkout
        juspayParams.putString(JuspayConstants.CLIENT_ID, Backend.CLIENT_ID);       //Your client ID for this platform with Express Checkout
        juspayParams.putString(JuspayConstants.ENVIRONMENT, Environment.PRODUCTION);//Set as PROD or Sandbox endpoint for Express Checkout API calls made in the SDK


        //Make a JSON array for the order IDs
        JSONArray orderArray = new JSONArray();

        //Make a JSON object for each delivery and put its order ID inside it
        for(Delivery d : deliveries) {
            try {
                JSONObject order = new JSONObject();
                order.put(JuspayConstants.ORDER_ID, d.orderId);
                orderArray.put(order);
            } catch (Exception e) {
                Log.e(LOG_TAG, "Failed to create JSON for order " + d.orderId, e);
            }
        }

        //Stringify the JSON array and add it to the params
        juspayParams.putString(JuspayConstants.CACHE_QR.ORDER_INFO, orderArray.toString());

        //Tell the SDK to only cache QRs and return
        juspayParams.putBoolean(JuspayConstants.CACHE_QR.ENABLE_QR_CACHING, true);


        //Wait for the caching to complete. If it fails, show an error message. If it succeeds,
        //update each delivery item in the UI to show a "QR cached" tag. If back pressed, do nothing
        showWaitingDialog("Caching QRs...");
        JuspayUPIPayment.start(this, juspayParams, cacheQrCallback);
    }


    //JuspayUPICallback for Caching QRs
    JuspayUPICallback cacheQrCallback = new JuspayUPICallback() {
        @Override
        public void onError(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    waitingDialog.dismiss();
                    showDialog("Failed to cache QRs");
                }
            });
        }

        @Override
        public void onBackPressed(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    waitingDialog.dismiss();
                }
            });
        }

        @Override
        public void onCompleted(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    waitingDialog.dismiss();
                    markQrsAsCached();
                }
            });
        }
    };



//------------------------------------- BACKEND ---------------------------------------------------


    private void getTodaysDeliveries() {
        Backend.getTodaysDeliveries(new Backend.DeliveriesListener() {
            @Override
            public void onReceived(final Delivery[] deliveries) {
                DeliveriesActivity.this.deliveries = deliveries;
                deliveryListAdapter = new DeliveryListAdapter(DeliveriesActivity.this, deliveries);
                waitingDialog.dismiss();
                ListView deliveriesList = findViewById(R.id.deliveries_list);
                deliveriesList.setAdapter(deliveryListAdapter);
                deliveriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        deliveryItemClick(deliveries[i], i);
                    }
                });
            }

            @Override
            public void onError(String error) {
                showDialog(error);
            }
        });
    }



    //Get payment status response from demo server and handle it.
    //Shows a dialog if payment is still pending OR starts PaymentConfirmationActivity to show a
    //Failure or Success message if the payment failed/succeeded
    private Backend.StatusListener statusListener = new Backend.StatusListener() {
        @Override
        public void onReceived(String status) {
            waitingDialog.dismiss();
            deliveries[lastOpenedDelivery].paymentStatus = status;
            if(status.equals(STATUS_CHARGED)) {
                DeliveriesActivity.this.showDialog("Payment Successful!");
            } else if(status.equals(STATUS_FAIL)) {
                DeliveriesActivity.this.showDialog("Payment Failed. Please try again");
            } else {
                DeliveriesActivity.this.showDialog("Could not confirm payment");
            }
            deliveryListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onError(String error) {
            waitingDialog.dismiss();
            DeliveriesActivity.this.showDialog("Something went wrong. Please try again");
        }
    };




//------------------------------------- UI HELPERS -------------------------------------------------



    //Set each delivery as cached and update their layout in the list to show that
    private void markQrsAsCached() {
        for(Delivery delivery : deliveries) {
            delivery.qrCached = true;
        }
        deliveryListAdapter.notifyDataSetChanged();
    }


    //Show a dialog with some message
    private void showDialog(String msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg);
        dialog.setCancelable(true);
        dialog.setNeutralButton("BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    //Show a waiting dialog with some message
    private void showWaitingDialog(String msg) {
        waitingDialog = new ProgressDialog(this);
        waitingDialog.setMessage(msg);
        waitingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Backend.cancel();
                finish();
            }
        });
        waitingDialog.setCanceledOnTouchOutside(false);
        waitingDialog.show();
    }

    //An invisible overlay layout is made visible to make the screen look dim.
    private void dimScreen() {
        final View overlay = findViewById(R.id.deliveries_overlay);
        overlay.animate().alpha(1).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationEnd(Animator animator) {
                overlay.setAlpha(1);
            }

            @Override
            public void onAnimationCancel(Animator animator) {}

            @Override
            public void onAnimationRepeat(Animator animator) {}
        }).start();
    }

    //The overlay layout is given a click listener to absorb and ignore any clicks until the sdk
    //starts up and takes over
    private void disableTouch() {
        View overlay = findViewById(R.id.deliveries_overlay);
        overlay.setVisibility(View.VISIBLE);
        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {}
        });
    }

    //Make the overlay invisible again and remove the dimming
    private void removeScreenDimmer() {
        final View overlay = findViewById(R.id.deliveries_overlay);
        overlay.animate().alpha(0).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationEnd(Animator animator) {
                overlay.setAlpha(0);
            }

            @Override
            public void onAnimationCancel(Animator animator) {}

            @Override
            public void onAnimationRepeat(Animator animator) {}
        }).start();
    }

    //Remove the overlay to allow touches in the activity again
    private void enableTouch() {
        final View overlay = findViewById(R.id.deliveries_overlay);
        overlay.setAlpha(0);
        overlay.setVisibility(View.GONE);
    }

    //Add the "Cache QRs" option to the overflow menu (top right)
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_deliveries_activity, menu);
        return true;
    }

    //Handle overflow menu option click (Cache Qrs)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.item_cache_qrs) {
            cacheAllQrs();
            return true;
        }
        return false;
    }
}
