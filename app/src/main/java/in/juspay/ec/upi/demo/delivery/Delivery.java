package in.juspay.ec.upi.demo.delivery;

public class Delivery {

    public final String orderId;
    public final String customerName;
    public final String address;
    public String description;
    public String paymentStatus;
    public boolean qrCached = false;

    public Delivery(String orderId, String customerName, String address, String description, String paymentStatus) {
        this.orderId = orderId;
        this.customerName = customerName;
        this.address = address;
        this.description = description;
        this.paymentStatus = paymentStatus;
    }
}
