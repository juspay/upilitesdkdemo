package in.juspay.ec.upi.demo.consumer;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import in.juspay.ec.upi.demo.Backend;
import in.juspay.ec.upi.demo.R;

import org.json.JSONObject;

import in.juspay.ec.upi.sdk.Environment;
import in.juspay.ec.upi.sdk.JuspayConstants;
import in.juspay.ec.upi.sdk.JuspayUPICallback;
import in.juspay.ec.upi.sdk.JuspayUPIPayment;

import static in.juspay.ec.upi.demo.Backend.STATUS_CHARGED;
import static in.juspay.ec.upi.demo.Backend.STATUS_FAIL;

/**
 *      This is an important activity. It gives the user a choice between paying with a UPI app
 *      (intent) or making a collect request and then starts the UPI SDK.
 *
 *      After the SDK returns, based on the type of callback, the Demo server will be contacted to
 *      confirm the payment status. (Demo server in turn checks with Express Checkout)
 *
 *      Use this class as an example for your integration with the SDK
 */


public class ChoosePaymentActivity extends AppCompatActivity {
    private static final String LOG_TAG = "ChoosePaymentActivity";

    private String orderId;
    private ProgressDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_payment);


        //Order ID for payment is passed here from CheckoutActivity
        this.orderId = getIntent().getStringExtra("order_id");


        //Set click listeners for the two payment options
        findViewById(R.id.pay_with_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { payWithApp(); }
        });
        findViewById(R.id.pay_with_collect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { payWithCollect(); }
        });


        //If user presses 'enter' key in the VPA text field, start the SDK for a collect request
        // (same as clicking the pay_with_collect button)
        findViewById(R.id.vpa).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                    payWithCollect();
                    return true;
                }
                return false;
            }
        });
    }



//--------------------------- INTENT PAYMENT ------------------------------------------------------


    //User clicked on "Pay with UPI app". Start the UPI SDK to make an Intent payment.
    private void payWithApp() {

        //Make a new bundle object. We will fill this with required params for intent
        Bundle juspayParams = new Bundle();

        //Tell the SDK to start an Intent method of payment
        juspayParams.putString(JuspayConstants.UPI_PAYMENT_METHOD, JuspayConstants.INTENT_CALL.LIB);


        //Add common params, prepare and start sdk (Defined below)
        startUPISdk(juspayParams);
    }



// ------------------------------ COLLECT REQUEST PAYMENT -----------------------------------------



    //User clicked on "Send a collect request". Start the UPI SDK to make a collect request
    private void payWithCollect() {

        //Make a new bundle object. We will fill this with required params for the collect request
        Bundle juspayParams = new Bundle();

        //Tell teh SDK to start a collect method of payment
        juspayParams.putString(JuspayConstants.UPI_PAYMENT_METHOD, JuspayConstants.COLLECT.LIB);

        //Get the user's VPA that they entered into the text field in UI
        juspayParams.putString(JuspayConstants.COLLECT.CUSTOMER_VPA, getVpaInput());

        //Add common params, prepare and start SDK (Defined below)
        startUPISdk(juspayParams);
    }






    //Add commons params and start the SDK
    private void startUPISdk(Bundle params) {

        //Required parameters
        params.putString(JuspayConstants.MERCHANT_ID, Backend.MERCHANT_ID);   //Your merchant ID with Express Checkout
        params.putString(JuspayConstants.CLIENT_ID, Backend.CLIENT_ID);       //Your client ID for this platform with Express Checkout
        params.putString(JuspayConstants.ORDER_ID, this.orderId);             //The Express Checkout order ID
        params.putString(JuspayConstants.ENVIRONMENT, Environment.PRODUCTION);//Set as PROD or Sandbox endpoint for Express Checkout API calls made in the SDK

        //Dim the screen
        dimScreen();

        //The SDK could take a second to start up. Prevent the user from tapping any buttons before it takes over the screen
        disableTouch();

        //Start the sdk with prepared bundle and callback object (defined below)
        JuspayUPIPayment.start(this, params, juspayUPICallback);
    }




// ---------------------------- CALLBACK OBJECT ---------------------------------------------------


    //Callback object to give SDK. When payment is done/aborted, SDK will call this object
    private JuspayUPICallback juspayUPICallback = new JuspayUPICallback() {

        //Called in case something abnormal happens during payment
        @Override
        public void onError(@Nullable JSONObject jsonObject) {
            String error = (jsonObject != null) ? jsonObject.toString() : "null"; //Check for null to be safe
            Log.e(LOG_TAG, "Juspay SDK error: " + error);

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {      //Callbacks can happen on another thread. Do your UI work on the UI thread
                @Override
                public void run() {
                    //Remove the screen dimmer and show a waiting dialog while we ask our Demo backend
                    // to confirm the payment status
                    removeScreenDimmer();
                    showWaitingDialog();
                }
            });

            //Ask demo merchant server to check payment status with Express Checkout
            Backend.getPaymentStatus(orderId, statusListener);
        }

        //Called in case the user pressed the back button and wanted to cancel the transaction
        @Override
        public void onBackPressed(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    removeScreenDimmer();
                    showWaitingDialog();
                }
            });

            //Ask demo merchant server to confirm payment status with Express Checkout
            Backend.getPaymentStatus(orderId, statusListener);
        }

        //Called when the payment is complete (Note: 'complete' here means the payment was either a success or a failure! Confirm with Express Checkout)
        @Override
        public void onCompleted(@Nullable JSONObject jsonObject) {

            //Callbacks can possibly happen on another thread. Do your UI work on the UI thread to avoid an exception
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    removeScreenDimmer();
                    showWaitingDialog();
                }
            });

            //Ask demo merchant server to confirm payment status with Express Checkout
            Backend.getPaymentStatus(orderId, statusListener);
        }
    };




//------------------------------------ BACKEND ---------------------------------------------------



    //Get payment status response from demo server and handle it.
    //Shows a dialog if payment is still pending OR starts PaymentConfirmationActivity to show a
    //Failure or Success message if the payment failed/succeeded
    private Backend.StatusListener statusListener = new Backend.StatusListener() {
        @Override
        public void onReceived(String status) {
            waitingDialog.dismiss();
            if(status.equals(STATUS_CHARGED)) {
                startPaymentConfirmationActivity(true);
            } else if(status.equals(STATUS_FAIL)) {
                startPaymentConfirmationActivity(false);
            } else {
                showErrorDialog("Could not confirm your payment");
            }
        }

        @Override
        public void onError(String error) {
            waitingDialog.dismiss();
            showErrorDialog("Something went wrong. Please try again");
        }
    };





//------------------------------------UI HELPERS-------------------------------------------------


    //An invisible overlay layout is made visible to make the screen look dim.
    private void dimScreen() {
        final View overlay = findViewById(R.id.overlay);
        overlay.animate().alpha(1).setDuration(500).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationEnd(Animator animator) {
                overlay.setAlpha(1);
            }

            @Override
            public void onAnimationCancel(Animator animator) {}

            @Override
            public void onAnimationRepeat(Animator animator) {}
        }).start();
    }



    //The overlay layout is given a click listener to absorb and ignore any clicks until the sdk
    //starts up and takes over
    private void disableTouch() {
        View overlay = findViewById(R.id.overlay);
        overlay.setVisibility(View.VISIBLE);
        overlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {}
        });
    }


    //Make the overlay invisible again and remove the dimming
    private void removeScreenDimmer() {
        final View overlay = findViewById(R.id.overlay);
        overlay.animate().alpha(0).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {}

            @Override
            public void onAnimationEnd(Animator animator) {
                overlay.setAlpha(0);
            }

            @Override
            public void onAnimationCancel(Animator animator) {}

            @Override
            public void onAnimationRepeat(Animator animator) {}
        }).start();
    }


    //Read the entered VPA from the edit text in the UI
    private String getVpaInput() {
        EditText editText = findViewById(R.id.vpa);
        return editText.getText().toString();
    }


    //Show a waiting dialog while confirming payment with demo server
    private void showWaitingDialog() {
        waitingDialog = new ProgressDialog(this);
        waitingDialog.setCancelable(false);
        waitingDialog.setMessage("Confirming your payment...");
        waitingDialog.setCanceledOnTouchOutside(false);
        waitingDialog.show();
    }


    //If payment was success/failure ConfirmationActivity is started to show an appropriate message
    //to the user
    private void startPaymentConfirmationActivity(boolean success) {
        Intent intent = new Intent(this, PaymentConfirmationActivity.class);
        intent.putExtra("status", success);
        startActivity(intent);
        if(success) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        finish();
    }


    //Show a dialog with any error message.
    private void showErrorDialog(String msg) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(msg);
        dialog.setCancelable(false);
        dialog.setNeutralButton("BACK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
        dialog.show();
    }
}
