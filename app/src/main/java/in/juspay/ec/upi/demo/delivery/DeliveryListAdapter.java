package in.juspay.ec.upi.demo.delivery;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import in.juspay.ec.upi.demo.R;

import java.util.ArrayList;
import java.util.Arrays;

import static in.juspay.ec.upi.demo.Backend.STATUS_CHARGED;
import static in.juspay.ec.upi.demo.Backend.STATUS_FAIL;

public class DeliveryListAdapter extends ArrayAdapter<Delivery> {

    public DeliveryListAdapter(@NonNull Context context, Delivery[] deliveries) {
        super(context, R.layout.delivery, new ArrayList<Delivery>(Arrays.asList(deliveries)));
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            convertView =  LayoutInflater.from(getContext()).inflate(R.layout.delivery, parent, false);
        }

        Delivery delivery = getItem(position);
        TextView textView = convertView.findViewById(R.id.order_id);
        textView.setText(delivery.orderId);

        textView = convertView.findViewById(R.id.customer_name);
        textView.setText(delivery.customerName);

        textView = convertView.findViewById(R.id.customer_address);
        textView.setText(delivery.address);

        textView = convertView.findViewById(R.id.payment_status);
        if(delivery.paymentStatus.equals(STATUS_FAIL)) {
            textView.setText("FAILED");
            textView.setTextColor(Color.parseColor("#da3f3f"));
        } else if(delivery.paymentStatus.equals(STATUS_CHARGED)) {
            textView.setText("PAID");
            textView.setTextColor(Color.parseColor("#3a8c00"));
        } else {
            textView.setText("UNPAID");
            textView.setTextColor(Color.parseColor("#adadad"));
        }

        View view = convertView.findViewById(R.id.qr_cached);
        if(delivery.qrCached) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }
}
