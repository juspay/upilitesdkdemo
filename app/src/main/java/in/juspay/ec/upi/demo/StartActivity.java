package in.juspay.ec.upi.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import in.juspay.ec.upi.demo.consumer.CheckoutActivity;
import in.juspay.ec.upi.demo.delivery.DeliveriesActivity;

/**
 *  This activity let's the user choose to start either the consumer app demo (Paying with intent or
 *  collect requests) or start the delivery app demo (Paying with QR code or collect)
 */
 public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //Set button click listeners
        findViewById(R.id.button_start_consumer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { startConsumerDemo(); }
        });
        findViewById(R.id.button_start_delivery).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { startDeliveryDemo(); }
        });
    }

    private void startConsumerDemo() {
        Intent intent = new Intent(this, CheckoutActivity.class);
        startActivity(intent);
    }

    private void startDeliveryDemo() {
        Intent intent = new Intent(this, DeliveriesActivity.class);
        startActivity(intent);
    }
}
