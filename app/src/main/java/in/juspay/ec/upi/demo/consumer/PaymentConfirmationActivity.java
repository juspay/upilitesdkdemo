package in.juspay.ec.upi.demo.consumer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.juspay.ec.upi.demo.R;


/**
 *      Shows a success/failure message based on payment status from demo server
 */
public class PaymentConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirmation);

        boolean success = getIntent().getBooleanExtra("status", false);
        TextView status = findViewById(R.id.status);
        TextView subtext = findViewById(R.id.subtext);
        ImageView successImage = findViewById(R.id.success);
        ImageView failImage = findViewById(R.id.fail);

        if(success){
            status.setText("Payment Successful!");
            subtext.setText("");
            successImage.setVisibility(View.VISIBLE);
        } else {
            status.setText("Payment Failed");
            subtext.setText("Something went wrong. Please Try again");
            failImage.setVisibility(View.VISIBLE);
        }
    }
}
