package in.juspay.ec.upi.demo.consumer;


/**
 *      Data Structure for an Order. The demo server will fetch these values in getOrderDetails()
 *      orderId is the important field. Rest are just for the UI of this demo app
 */
public class Order {
    public final String orderId;
    public final String itemName;
    public final String itemId;
    public final String price;

    public Order(String orderId, String itemName, String itemId, String price) {
        this.orderId = orderId;
        this.itemName = itemName;
        this.itemId = itemId;
        this.price = price;
    }
}
